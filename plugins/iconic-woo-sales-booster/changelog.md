**v1.14.0** (26 Apr 2023)  
[new] Compatibility with YITH Added To Cart Pop-up  
[new] Hooks: `iconic_wsb_before_checkout_button_in_after_add_to_cart_modal`, `iconic_wsb_before_view_cart_link_in_after_add_to_cart_modal` and `iconic_wsb_after_view_cart_link_in_after_add_to_cart_modal`  
[fix] "Added to the cart" notice when Frequently Bought Together items are added  
[fix] After Checkout popup when the order is updated on the checkout page  

**v1.13.1** (02 Jan 2023)  
[fix] After Add to Cart Modal when the option `Enable AJAX for "Add Selected to Cart" button?` is checked  
[fix] Buttons alignment in the 'Customers Also Bought' modal  
[fix] Frequently Bought Together product variations not being added to the cart  

**v1.13.0** (09 Aug 2022)  
[new] Action to duplicate Order Bumps  
[new] Export and import Iconic Sales Booster for WooCommerce data  
[new] Allow changing the order bump status  
[new] Filter `iconic_wsb_fbt_thumbnail_size` to change the thumbnail size in the FBT section  
[fix] Frequently Bought Together data copied by WPML  

**v1.12.0** (27 Jul 2022)  
[new] Compatibility with WooCommerce Multilingual & Multicurrency  
[fix] Frequently Bought Together when AJAX for "Add Selected to Cart" is disabled  
[fix] Number of offers shown in the After Add to Cart Modal  
[fix] Modal content after adding another product within the modal  
[fix] Compatibility issue with After Add to Cart Modal and page builders  

**v1.11.0** (27 Jun 2022)  
[new] Compatibility with WooCommerce eCurring gateway  
[new] Compatibility with WooCommerce Subscriptions  
[fix] Product variation offer when the condition is a product variation  
[fix] Image and name of the variation product on the After Add to Cart Modal  

**v1.10.0** (19 May 2022)  
[new] Order Bump shortcode `[iconic_wsb_order_bump]`  
[fix] Show the popup on the cart page if the option "Redirect to the cart page after successful addition" is enabled  
[fix] Product variations unavailable on the product page  

**v1.9.0** (25 Apr 2022)  
[new] Compatibility with Variation Swatches for WooCommerce by RadiusTheme  
[fix] Adding products to the cart when order bump is using "After Order Review" or "After Checkout Form" position  
[fix] Offered product to be added if it belongs to the excluded category added on the order bump condition  
[fix] Prevent PHP notice when preparing data to be added to the cart  

**v1.8.0** (4 Apr 2022)  
[fix] 'Frequently Bought Together' and 'Customers Also Bought' translation issue with WPML plugin   
[fix] Product variation titles on product search dropdown  
[fix] Prevent possible PHP warning on getting settings  
[fix] Adding "Frequently Bought Together" products multiple times  

**v1.7.0** (1 Mar 2022)  
[new] Getting started onboarding section  
[update] New setting to allow "frequently bought together" discount to be applied when items are added separately  
[fix] Allow "frequently bought together" products to be added multiple times  

**v1.6.0** (1 Mar 2022)  
[fix] Allow FBT parent item to be added seperately  
[fix] Fix none unique array when using FBT  
[Fix] Issue where order bump would not work when "only" condition is used  
[fix] Security fix  

**v1.5.0** (8 Dec 2021)  
[new] After Add to Cart modal can appear on archive pages  
[new] Filter order bump and after checkout cross sales by category and  introduced 'only' and 'none' sub filters  
[fix] Issue with Front End Asset Load  

**v1.4.0** (13 Oct 2021)  
[new] Additional locations for Frequently Bought Together  
[fix] FBT Panel updates when item added  
[fix] Order Bump not Accepting Decimal Discounts  
[fix] Fatal Error when setting session  
[fix] Offer text not applied to at checkout bumps  
[fix] Error if discount left empty  

**v1.3.0** (28 Sept 2021)  
[new] Option to show after add to cart modal when there are no cross sells  
[new] Allow order bump to include more of the same product  
[fix] If multiple items cannot be added to the cart, only last message is shown  
[fix] Ajax woo product search results not in correct format  
[fix] Fix warning when adding a product to cart through Order Bump  
[fix] Prepend Offer text to duplicate item in checkout  

**v1.2.0** (23 Aug 2021)  
[new] Added filter `iconic_wsb_order_bump_image_size`  
[new] Additional hooks for order bump  
[fix] Add support for subscription product variations to order bumps and after checkout popup  
[fix] Product variation changes not updating Frequently Bought Together  
[fix] Remove the discounted product added with order bump if that's the only product in cart    
[fix] Variation Product added to Frequently Bought Together showing empty dropdown  
[fix] Prevent item from showing in after cart popup if already in cart  
[fix] Bring success messages inline with default Woocommerce  
[fix] Sales bump removes itself on same product  
[fix] Better formatting of product selectors  
[fix] Sometimes the same cart item would be listed twice instead of increasing quantity  

**v1.1.6** (6 May 2021)  
[update] Allow decimal discount values  
[update] Allow comma as decimal seperator  
[update] Remove markup from the price in FBT discount message  
[update] Allow empty value in FBT discount field  
[update] Add success/error notice for Frequently Bought Together AJAX call  
[update] Update dependencies  
[fix] Fix issue with variations in After Checkout popup  
[fix] FBT: Fix price not updating issue  
[fix] FBT: Tax exclusive discount for "other" locations  
[new] Option to show Order Bump even if the offer product is already in the cart  
[new] FBT: set unchecked setting  
[fix] Fix compatibility issue with WooCommerce Attribute Swatches by IconicWP  
[fix] Ensure variation name is used when creating an order bump or after checkout offer  
[fix] Loco translate compatibility  
[fix] FBT: Fix issue with adding products with custom attributes to the cart  

**v1.1.5** (19 Aug 2020)  
[new] Ability to change title of "frequenty bought together" products, per-product  
[update] Add compatibility with Flux Checkout  
[update] Add new filter `iconic_wsb_order_bump_position`  
[fix] Compatibility with WP 5.5 and jQuery v1.12.4 excluding jQuery Migrate 1.x  
[fix] Handle non-latin characters in terms and taxonomies  
[fix] Fix checkout bump issue when discount is 100 percent  

**v1.1.4** (18 Mar 2020)  
[update] Version compatibility  

**v1.1.3** (19 Feb 2020)  
[new] Setting to show hidden products in FBT  
[update] Add .pot file  
[update] AJAX for Frequently Bought Together  
[update] Show/hide images in Frequently Bought Together  
[update] Sales Pitch  
[update] Show alert when clicked on button 'Add selected to cart' in FBT  
[update] Update dependencies  
[fix] Ensure decimals are counted in FBT totals  
[fix] Issue with non-existent FBT products  
[fix] Order Bump not showing at checkout in WooCommerce 3.9+  
[fix] Prevent PHP warning in FBT  
[fix] Issue that prevented removing variable products from order bump at checkout  
[fix] After Add to cart Popup: Show price, image and title from the child/variation product  
[fix] Issue with pricing when tax settings were enabled  
[fix] FBT label bug for Non-English locale bug  
[fix] Sorting issue  

**v1.1.2** (29 Nov 2019)  
[update] Allow hidden products in bumps  
[fix] Call to undefined method get_variation_attributes()  

**v1.1.1** (21 Nov 2019)  
[update] Auto select order bump checkbox after choosing a variation  
[fix] Ensure FBT calculations are correct when tax is involved  
[fix] Ensure variation is selected when adding from FBT  
[fix] Fix pricing in "customers also bought" modal and cart count  
[fix] Use own AJAX Url  

**v1.1.0** (4 Nov 2019)  
[new] Offer variable products and variations within all cross-sell areas  
[new] Offer discounts for "frequently bought together" products  
[update] Add compatibility with WooCommerce Attribute Swatches by Iconic  
[update] Update dependencies  
[fix] Save cross-sells when product edit field is empty  
[fix] Trashed products should no longer be shown in bumps  
[fix] Ensure tax is accounted for (if enabled) on After Checkout and Order Bumps  
[fix] Fix issue preventing WP CLI commands from running  

**v1.0.1** (1 July 2019)  
[Fix] Updated Freemius integration and dependencies.  

**v1.0.0** (3 June 2019)  
Initial Release
