<?php
/**
 * Popup related products template
 *
 * @author  YITH
 * @package YITH WooCommerce Added to Cart Popup
 * @version 1.0.0
 */

defined( 'YITH_WACP' ) || exit; // Exit if accessed directly.

global $product, $woocommerce_loop;

$loop = 0;
$args = apply_filters(
	'yith_wacp_related_products_args',
	array(
		'post_type'           => array( 'product', 'product_variation' ),
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'no_found_rows'       => 1,
		'posts_per_page'      => $posts_per_page,
		'post__in'            => $items,
		'post__not_in'        => array( $current_product_id ),
		'orderby'             => 'rand',
	)
);

$products = new WP_Query( $args );

if ( $products->have_posts() ) : ?>
	<div class="ph-scroller">
	<div class="woocommmerce yith-wacp-related">

		<h3><?php echo esc_html( $title ); ?></h3>

		<ul class="products columns-<?php echo esc_attr( $columns ); ?>">

			<?php

			// Extra post classes.
			$classes = array( 'yith-wacp-related-product' );
			// Set columns.
			$woocommerce_loop['loop']    = 0;
			$woocommerce_loop['columns'] = $columns;

			while ( $products->have_posts() ) :
				$products->the_post();
				?>

				<li <?php post_class( $classes ); ?>>

					<?php do_action( 'yith_wacp_before_related_item' ); ?>

					

						<div class="product-image">
							<a href="<?php the_permalink(); ?>">
							<?php
							wc_get_template( 'loop/sale-flash.php' );
							$image_size = apply_filters( 'yith_wacp_suggested_product_image_size', 'shop_catalog' );
							echo woocommerce_get_product_thumbnail( $image_size ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
							?>
							</a>
						</div>

						<h3 class="product-title">
							<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
							</a>
						</h3>

						<div class="product-price">
							<?php wc_get_template( 'loop/price.php' ); ?>
						

						<?php
						if ( $show_add_to_cart ) {
							$_id = get_the_ID();
							$produs = wc_get_product($_id);
							$return = '<a class="button view-product-dbx mobile_atc popup add_to_cart_button ajax_add_to_cart" href="' . $produs->add_to_cart_url() . '" data-product_id="'.$_id.'" data-product_sku="'.$produs->get_sku().'" rel="nofollow"><img class="btn-icon-atc mobile_atc_img" src="/wp-content/uploads/2022/11/shopping-cart.png"/>'.'</a>';
							echo $return;
							//echo do_shortcode( '[add_to_cart id="' . get_the_ID() . '" style="" show_price="false"]' );
						}
						?>
						</div>
					

					<?php do_action( 'yith_wacp_after_related_item' ); ?>

				</li>

			<?php endwhile; // end of the loop. ?>

		</ul>

	</div>
	</div>
	<?php
endif;

wp_reset_postdata();
