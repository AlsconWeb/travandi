<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 7.0.1
 */

defined( 'ABSPATH' ) || exit;

$row_classes     = array();
$main_classes    = array();
$sidebar_classes = array();

$auto_refresh  = get_theme_mod( 'cart_auto_refresh' );
$row_classes[] = 'row-large';
$row_classes[] = 'row-divided';

if ( $auto_refresh ) {
	$main_classes[] = 'cart-auto-refresh';
}


$row_classes     = implode( ' ', $row_classes );
$main_classes    = implode( ' ', $main_classes );
$sidebar_classes = implode( ' ', $sidebar_classes );


do_action( 'woocommerce_before_cart' ); ?>
<div class="woocommerce row <?php echo $row_classes; ?>">
<div class="col large-8 pb-0 <?php echo $main_classes; ?>">
    <h2 class="cart-title">Coșul meu</h2>
	<div class="cart-top-mobilez">
	<div class="cart-top-mobile">
		<div class="top-total-text">
			<span>Total:</span>
		</div>
		<div class="top-total-total">
		<?php echo wc_cart_totals_order_total_html();?>
		</div>
	</div>
	<div class="wc-proceed-to-checkout-top">
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
	</div>
	</div>
<?php wc_print_notices(); ?>
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
<div class="cart-wrapper sm-touch-scroll">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
	<table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					if(wp_is_mobile() ){
						?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						<td class="product-thumbnail">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
						if ( ! $product_permalink ) {
							echo $thumbnail; // PHPCS: XSS ok.
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
						}
						?>
						</td>
						<td class="product-details" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
						<div class="product-name">
							<?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
						}
						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
						}
						?>
						</div>
						<div class="detalii-produs-cos">
						<div class="detalii-produs-col last">
							<div class="pret">
						        <?php
							$qty = $cart_item['quantity'];
							$regular_price = $_product->get_regular_price();
							$sale_price = $_product->get_sale_price();
							$pret = $_product->get_price();
							$total = $qty * $pret;
							$total_regular = $qty * $regular_price;
							$econom = $total_regular - $total;
							if($sale_price){
							    echo '<div class="total-produs double">';
								echo '<div class="old-totalz">'.wc_price($total_regular * 1.19).'</div>';
								echo wc_price($total * 1.19);
							    echo '</div>';
							    
							}
							else{
							    echo '<div class="total-produs">';
							    echo wc_price($total * 1.19);
							    echo '</div>';
							}  
							?>
							</div>
							<div class="actiuni-produs">
							<?php
						$qty = $cart_item['quantity'];
						$pret = $_product->get_price();
						$total = $qty * $pret;
						if($total != 0){
						echo apply_filters('woocommerce_cart_item_remove_link',
								sprintf(
								'<a href="%s" class="remove-product" aria-label="%s" data-product_id="%s" data-product_sku="%s">Șterge</a>',
								esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
								esc_html__( 'Remove this item', 'woocommerce' ),
								esc_attr( $product_id ),
								esc_attr( $_product->get_sku() )
								),
								$cart_item_key
						);
						}
						?>  
							</div>	
						</div>	
						<div class="detalii-produs-col first">
							<div class="disponibilitate">
								<?php
								$stock_status = $_product->get_stock_status();
								$stock_qty = $_product->get_stock_quantity();
								switch($stock_status){
									case 'instock':
										if($stock_qty && ($stock_qty < 5)){
											$stock_text = 'Ultimele bucăți';
											$badge_class = 'last';
										}
										else{
											$stock_text = 'În stoc';
											$badge_class = 'instock';
										}
									break;	
									case 'onbackorder':
										$stock_text = 'Verificare stoc';
										$badge_class = 'verificare';
									break;		
								}
								$stock_badge = '<span class="stock_badge '.$badge_class.'">'.$stock_text.'</span>';
								echo $stock_badge;
								?>
							</div>
							<div class="cantitate">
							<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input(
								array(
									'input_name'   => "cart[{$cart_item_key}][qty]",
									'input_value'  => $cart_item['quantity'],
									'max_value'    => $_product->get_max_purchase_quantity(),
									'min_value'    => '0',
									'product_name' => $_product->get_name(),
								),
								$_product,
								false
							);
						}
						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); 
						?> 
							</div>
						</div>	
						</div>	
						<div class="product-details-bottom">
						</div>	
						</td>
					</tr>
					<?php
					}
					else{
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
						<td class="product-thumbnail">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
						if ( ! $product_permalink ) {
							echo $thumbnail; // PHPCS: XSS ok.
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
						}
						?>
						</td>
						<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'woocommerce' ); ?>">
						<?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
						}

						do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

						// Meta data.
						echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

						// Backorder notification.
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
						}
						?>
						<div class="product-details">
						    <div class="pret-produs">
						    <div class="pret">
							<?php if($_product->get_price() > 0) { ?>
						        <?php if($_product->get_sale_price()) { ?>
						        <del><?php echo wc_price($_product->get_regular_price() * 1.19); ?></del>
						        <ins><?php echo wc_price($_product->get_price() * 1.19); ?></ins>
						        <?php } else { ?>
						        <ins><?php echo wc_price($_product->get_price() * 1.19); ?></ins>
						        <?php } }?>
						    </div>
						    <div class="disponibilitate">
								<?php
								$stock_status = $_product->get_stock_status();
								$stock_qty = $_product->get_stock_quantity();
								$qty = $cart_item['quantity'];
								$pret = $_product->get_price();
								$total = $qty * $pret;
								switch($stock_status){
									case 'instock':
										if($stock_qty && ($stock_qty < 5)){
											$stock_text = 'Ultimile bucăți';
											$badge_class = 'last';
										}
										else{
											$stock_text = 'În stoc';
											$badge_class = 'instock';
										}
									break;	
									case 'onbackorder':
										$stock_text = 'Verificare stoc';
										$badge_class = 'verificare';
									break;		
								}
								$stock_badge = '<span class="stock_badge '.$badge_class.'">'.$stock_text.'</span>';
								if($total > 0){
									echo $stock_badge;
								}
								?>
							</div>
							</div>
							<div class="cantitate-produs">
							    <div class="pret-title">Cantitate: </div>
							<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input(
								array(
									'input_name'   => "cart[{$cart_item_key}][qty]",
									'input_value'  => $cart_item['quantity'],
									'max_value'    => $_product->get_max_purchase_quantity(),
									'min_value'    => '0',
									'product_name' => $_product->get_name(),
								),
								$_product,
								false
							);
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
						?>    
							</div>
						</div>	
						</td>
						

						<td class="product-subtotal" data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>">
							<?php
							$qty = $cart_item['quantity'];
							$regular_price = $_product->get_regular_price();
							$sale_price = $_product->get_sale_price();
							$pret = $_product->get_price();
							$total = $qty * $pret;
							$total_regular = $qty * $regular_price;
							$econom = $total_regular - $total;
							$econom = (float)$econom * 1.19;

							if($total != 0){
							if($sale_price){
								?>
								<div class="total-produs">
									<?php echo wc_price($total * 1.19);?>
								</div>
								<div class="econom-produs">
								<div class="old-total"><?php echo wc_price($total_regular * 1.19);?></div>
								<div class="econom-title">Economisești:</div>
								<div class="econom-value"><?php echo wc_price($econom);?></div>
								</div>
								<?php
							}
							else{
								?>
								<div class="total-produs"><?php echo wc_price($total * 1.19);?></div>
								<?php
							}  
							}
							?>
						<div class="product-actions">
						<?php
						$qty = $cart_item['quantity'];
						$pret = $_product->get_price();
						$total = $qty * $pret;
						if($total != 0){
						echo apply_filters('woocommerce_cart_item_remove_link',
								sprintf(
								'<a href="%s" class="remove-product" aria-label="%s" data-product_id="%s" data-product_sku="%s">Șterge</a>',
								esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
								esc_html__( 'Remove this item', 'woocommerce' ),
								esc_attr( $product_id ),
								esc_attr( $_product->get_sku() )
								),
								$cart_item_key
						);
						}
						?>        
						</div>	
						</td>
						<td class="product-subtotal-mobile" data-title="<?php esc_attr_e( 'Subtotal', 'woocommerce' ); ?>">
							<?php
							$qty = $cart_item['quantity'];
							$regular_price = $_product->get_regular_price();
							$sale_price = $_product->get_sale_price();
							$pret = $_product->get_price();
							$total = $qty * $pret;
							$total_regular = $qty * $regular_price;
							
							if($total != 0){
							    if($sale_price){
									$total_free = $qty * $sale_price;
							$econom = $total_regular - $total;
							$econom_free = $total_free - $total;
							    echo '<div class="total-produs">';
							    echo '<span class="total-title">Total:</span>';
							    echo '<span class="old-total">'.wc_price($total_regular * 1.19).'</span>';
							    echo '<span class="total-price">'.wc_price($total * 1.19).'</span>';
							    echo '</div>';
							    echo '<div class="econom-produs">';
							    echo '<div class="econom-title">Economisești:</div>';
							    echo '<div class="econom-value">'.wc_price($econom * 1.19).'</div>';
							    echo '</div>';
							    }
							    else{
							    echo '<div class="total-produs">';
							    echo '<span class="total-title">Total:</span>';
							    echo '<span class="total-price">'.wc_price($total * 1.19).'</span>';
							    echo '</div>';
							    }     
							}
							else {
							    echo '<div class="total-produs gift-div">';
							    echo '<div class="cart-gift-nt"><img src="/wp-content/uploads/2021/08/gift-box.svg" />'.'<span>Produs Cadou</span></div>';
							    echo '</div>';    
							}
							?>
						<div class="product-actions">
						<?php
						$qty = $cart_item['quantity'];
						$pret = $_product->get_price();
						$total = $qty * $pret;
						if($total != 0){
						echo apply_filters('woocommerce_cart_item_remove_link',
								sprintf(
								'<a href="%s" class="remove-product" aria-label="%s" data-product_id="%s" data-product_sku="%s">Șterge</a>',
								esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
								esc_html__( 'Remove this item', 'woocommerce' ),
								esc_attr( $product_id ),
								esc_attr( $_product->get_sku() )
								),
								$cart_item_key
						);
						}
						?>               
						</div>	
						</td>
					</tr>
					<?php
				}
			}
			}
			?>

			<?php do_action( 'woocommerce_cart_contents' ); ?>

			<tr>
				<td colspan="6" class="actions clear">

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<button type="submit" class="button primary mt-0 pull-left small" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
				</td>
			</tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tbody>
	</table>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</div>
</form>
</div>

<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

<div class="cart-collaterals large-4 col pb-0">
	<?php flatsome_sticky_column_open( 'cart_sticky_sidebar' ); ?>

	<div class="cart-sidebar col-inner <?php echo $sidebar_classes; ?>">
		<?php
			/**
			 * Cart collaterals hook.
			 *
			 * @hooked woocommerce_cross_sell_display
			 * @hooked woocommerce_cart_totals - 10
			 */
			do_action( 'woocommerce_cart_collaterals' );
		?>
		<?php if ( wc_coupons_enabled() ) { ?>
		<form class="checkout_coupon mb-0" method="post">
			<div class="coupon">
				<h3 class="widget-title"><?php echo get_flatsome_icon( 'icon-tag' ); ?> <?php esc_html_e( 'Ai un cupon?', 'woocommerce' ); ?></h3><input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Cod cupon', 'woocommerce' ); ?>" /> <input type="submit" class="is-form expand" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" />
				<?php do_action( 'woocommerce_cart_coupon' ); ?>
			</div>
		</form>
		<?php } ?>
		<?php do_action( 'flatsome_cart_sidebar' ); ?>
	</div>

	<?php flatsome_sticky_column_close( 'cart_sticky_sidebar' ); ?>
</div>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
