<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 7.0.1
 */
defined( 'ABSPATH' ) || exit;
do_action( 'woocommerce_before_account_orders', $has_orders );
echo '<div class="dbx_account_orders">';
echo '<h2>Comenzile mele</h2>';
if ( $has_orders ){
foreach ( $customer_orders->orders as $customer_order ) {
	$order      = wc_get_order( $customer_order ); 
	$item_count = $order->get_item_count() - $order->get_item_count_refunded();
	$view_order = esc_url( $order->get_view_order_url());
	$order_no = esc_html( $order->get_order_number() );
	$date_created = $order->get_date_created();
	$status = wc_get_order_status_name( $order->get_status() );
	$total = $order->get_formatted_order_total();
	$actions = wc_get_account_orders_actions( $order );
	echo '<div class="account_order_dbx">';
	echo '<div class="account_order_top">';
	echo '<p class="account_order_title"><a href="'.$view_order.'">Comanda nr. '.$order_no.'</a><span class="status_comanda">'.$status.'</span></p>';
	echo '<p>Plasată pe '.$date_created->date("d/m/Y, H:i").' | '.'Total: '.$total.'</p>';
	echo '</div>';
	echo '<div class="account_order_actions">';
	if ( ! empty( $actions ) ) {
		foreach ( $actions as $key => $action ) {
			$_action = $action['name'];
			if($action['name'] == 'Vezi'){
				$_action = 'Detalii Comandă';
			}
		echo '<a href="' . esc_url( $action['url'] ) . '" class="order_action_button ' . sanitize_html_class( $key ) . '">' . esc_html( $_action ) . '</a>';
		}
	}
	echo '</div>';
	echo '</div>';
}
do_action( 'woocommerce_before_account_orders_pagination' );
if ( 1 < $customer_orders->max_num_pages ){
	echo '<div class="dbx_orders_pagination">';
	if ( 1 !== $current_page ){
		echo '<a class="dbx_nav_btn" href="'.esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) ).'"> « Înapoi </a>';
	}
	echo '<span>'.$current_page.'/'.$customer_orders->max_num_pages.'</span>';
	if ( intval( $customer_orders->max_num_pages ) !== $current_page ){
		echo '<a class="dbx_nav_btn" href="'.esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) ).'"> Înainte »</a>';
	}	
	echo '</div>';
}
echo '</div>';
}
else{
	echo '<div class="account_order_dbx">';
	echo '<p>Nu aveți înregistrată nici o comandă.</p>';
	echo '<a href="'.esc_url( wc_get_page_permalink( 'shop' ) ).'" class="order_action_button">Toate Produsele</a>';
	echo '</div>';
}
do_action( 'woocommerce_after_account_orders', $has_orders );
?>