<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 6.0.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<?php echo '<div class="email_top_text" ><div style="height: 200px;vertical-align: middle;padding: 50px 0;">';?>
<?php /* translators: %s: Customer username */ ?>
<p style="text-align: left!important; color:#000!important; font-size: 15px;"><?php printf( esc_html__( 'Salut %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>
<p style="text-align: left!important; color:#000!important; font-size: 15px;"><?php printf( esc_html__( 'Îți mulțumim pentru crearea contului pe %1$s! Numele tău de utilizator este: %2$s. Poți intra în contul tău de aici: %3$s', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated && $set_password_url ) : ?>
	<?php // If the password has not been set by the user during the sign up process, send them a link to set a new password ?>
	<p style="text-align: left!important; color:#000!important; font-size: 15px;"><a href="<?php echo esc_attr( $set_password_url ); ?>"><?php printf( esc_html__( 'Click aici pentru a seta o parolă.', 'woocommerce' ) ); ?></a></p>
<?php endif; ?>

<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 */
/*
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}
*/
echo '</div>';
do_action( 'woocommerce_email_footer', $email );
