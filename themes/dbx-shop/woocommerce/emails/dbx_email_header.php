<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$site_url = get_bloginfo('url');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
	</head>
	<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
		<div class="dbx-email" align="center" valign="top">	
		<div class="dbx-email-container">
			<div class="email-head-dbx">
				<div class="email-logo">
					<?php if ( $img = get_option( 'woocommerce_email_header_image' ) ) { ?>
					<a href="<?php echo $site_url;?>"><img src="<?php echo esc_url( $img );?>" /></a>
					<?php } ?>
				</div>
				<?php if ($order_no) {?>
				<div class="email-order-no-top">
					<span>Comanda ta</span>
					<span><b>#<?php echo $order_no;?></b></span>
				</div>
				<?php } ?>
			</div>
			<div class="divider-dbx"></div>