<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	$order_id = $order->get_id();
	$documente = dbx_check_documents_per_order($order_id);
	if(!empty($documente)){
		$curier = $documente['0']->curier;
		$nr_awb = $documente['0']->nr_awb;
		$link_factura = $documente['0']->link_factura;
	}
	$modalitate_plata = $order->get_payment_method_title();
	$country = $order->get_billing_country();
	$state = $order->get_billing_state();
	$country2 = $order->get_shipping_country();
	$state2 = $order->get_shipping_state();
	$shipping_phone = $order->get_shipping_phone();
	$billing_phone = $order->get_billing_phone();
	if(empty($shipping_phone)){
		$shipping_phone = $billing_phone;
	}
	$judet_facturare = WC()->countries->get_states( $country )[$state];
	$judet_livrare = WC()->countries->get_states( $country2 )[$state2];
	$oras_facturare = $order->get_billing_city();
	$oras_livrare = $order->get_shipping_city();
	$firma = $order->get_billing_company();
	$cui = get_post_meta($order_id, 'cui', true);
	$reg_com = get_post_meta($order_id, 'nr_reg_com', true);
	$adresa_facturare = $order->get_billing_address_1().', '.$oras_facturare.', '.$judet_facturare;
	$adresa_livrare = $order->get_shipping_address_1().', '.$oras_livrare.', '.$judet_livrare;
	$nume_livrare = $order->get_shipping_first_name().' '.$order->get_shipping_last_name();
	$nume_facturare = $order->get_billing_first_name().' '.$order->get_billing_last_name();
	switch($modalitate_plata){
		case 'Ramburs la curier':
			$next = 'Vei plăti produsele în momentul ridicării coletului.';
			break;
		case 'Plăteşte cu cardul':
			$next = 'Plata cu cardul a fost acceptată și în curând te vei putea bucura de produsele pe care le-ai ales.';
			break;	
	}
	$order_no = $order->get_order_number();
	$url_tracking = 'https://dragonstarcurier.ro/verificare-awb/';
	if($curier == 'Urgent Cargus'){
		$url_tracking = 'https://app.urgentcargus.ro/Private/Tracking.aspx?CodBara='.$nr_awb;
	}
/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email, $order_no );
?>
<div class="email_top_text">
	<div style="padding: 15px;">
	<p style="font-size: 16px!important;font-weight:bold!important;">Produsele au fost predate curierului.</p>
	<p>Le vei primi curând, la adresa specificată.</p>
	<h2 style="color: #000;padding: 5px 0;">Detalii expediere:</h2>
	<p style="text-align: left!important;">Curier: <span style="color: #000!important;"><?php echo $curier;?></span></p>
	<p style="text-align: left!important;">Nr. AWB: <span style="color: #000!important;"><?php echo $nr_awb;?></span></p>	  
	<p style="text-align: left!important;"><a href="<?php echo $url_tracking;?>" target="_blank" style="font-size: 15px!important;padding: 7px 20px;background: #EA2024;border-radius: 21px;color: #fff;width: fit-content;">Urmărește AWB</a></p>		
	</div>
	<div class="divider-dbx"></div>
	<div class="produse_email" stlye="padding: 10px;">
	<h2 style="color: #000;padding: 5px 0;">Conținut comandă</h2>	
	<?php
	echo '<table style="background:#ffffff;border-collapse:collapse" width="100%" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF"><tbody>';
	$items = $order->get_items();
	$total_produse = 0;	
	$transport = $order->get_shipping_total();
	foreach($items as $key=>$item){
		$product = $item->get_product();
		if(!$product) continue;
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'single-post-thumbnail' );
		$_img = $image['0'];
		$url = $product->get_permalink();
		$product_name = $item->get_name();
		$quantity = $item->get_quantity();
		$subtotal = $item->get_subtotal();
		$_tax = $item->get_subtotal_tax();
   		//$total = ($subtotal + $tax) * $quantity);
		$_total = $item->get_total();
		$total_produs = ($_total + $_tax);
		?>
		<tr style="border-bottom: 1px solid #33333329;">
<td align="left" valign="middle" width="60" style="padding: 5px 0;"><a style="text-decoration:none;color:#000000" href="<?php echo $url; ?>"> <img style="width:100%;max-width:60px" src="<?php echo $_img; ?>" width="60" border="0"></a></td>
<td style="padding-left:8px;font-size:13px;color:#5a5a5a;font-family:'Open Sans',sans-serif,Helvetica,Arial" align="left" valign="middle"><a style="text-decoration:none;font-size:13px;color:#5a5a5a;font-family:'Open Sans',sans-serif,Helvetica,Arial" href="<?php echo $url; ?>"> <?php echo $product_name; ?></a></td>
<td style="text-align:right;font-size:13px;color:#5a5a5a;font-family:'Open Sans',sans-serif,Helvetica,Arial" align="right" valign="middle" width="50"><?php echo $quantity;?> buc</td>
<td style="text-align:right;font-size:13px;color:#5a5a5a;font-family:'Open Sans',sans-serif,Helvetica,Arial" valign="middle" width="90"><?php echo wc_price($total_produs);?></td>
		</tr>
		<?php
	}
	if($transport > 0){
		$_t = wc_price($transport);
	}
	else{
		$_t = 'Gratuit';
	}
	$g1 = $order->get_total();
	$total_tax = $order->get_total_tax();
	echo '</tbody></table>';
	echo '<div class="total_livrare" style="text-align: right;">Cost livrare: <b>'.$_t.'</b></div>';	
	echo '<div class="total_produse" style="text-align: right;">Total: <b>'.wc_price($g1).'</b><br/><small>(include '.$total_tax.' lei TVA)</small></div>';	
		//echo $order->get_line_subtotal().' | '.$order->get_line_tax().' | '.$order->get_line_total();
	?>	
	</div>	
	<div class="divider-dbx"></div>
	<div class="detalii_comanda" stlye="padding: 10px;">
	<h2 style="color: #000; padding: 5px 0;">Detalii facturare:</h2>
		<?php if($firma): ?>
		<p style="text-align: left!important;">Nume Firmă: <span style="color: #000!important;"><?php echo $order->get_billing_company();?></span></p>
		<?php endif;?>
		<p style="text-align: left!important;">Nume: <span style="color: #000!important;"><?php echo $nume_facturare;?></span></p>
		<?php if($cui ): ?>
		<p style="text-align: left!important;">CUI: <span style="color: #000!important;"><?php echo $cui;?></span></p>
		<?php endif;?>
		<?php if($reg_com ):?>
		<p style="text-align: left!important;">REG. COM.: <span style="color: #000!important;"><?php echo $reg_com;?></span></p>
		<?php endif;?>
		<p style="text-align: left!important;">Adresa: <span style="color: #000!important;"><?php echo $adresa_facturare;?></span></p>
		<p style="text-align: left!important;">Telefon: <span style="color: #000!important;"><?php echo $billing_phone;?></span></p>
	<h2 style="color: #000; padding: 5px 0;">Detalii livrare:</h2>
		<p style="text-align: left!important;">Persoană de contact: <span style="color: #000!important;"><?php echo $nume_livrare;?></span></p>
		<p style="text-align: left!important;">Adresa: <span style="color: #000!important;"><?php echo $adresa_livrare;?></span></p>
		<p style="text-align: left!important;">Telefon: <span style="color: #000!important;"><?php echo $shipping_phone;?></span></p>
	</div>
</div>
<?php
do_action( 'woocommerce_email_footer', $email );
?>