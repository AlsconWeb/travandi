<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="divider-dbx"></div>
		</div>
		</div>
		<div class="footer_abs" style="text-align: center; font-size: 10px; color: #000;">
		Copyright © <?php echo do_shortcode('[ux_current_year]');?> Activ Travandi SRL,<br/> CUI: RO23910510, <br/>Reg. Com.:J51/413/2008 <br/>			<span class="social-media-footer">
		<a href="https://www.facebook.com/travandi" target="_blank"><img style="width: 20px;" src="<?php echo get_bloginfo('url') ?>/wp-content/uploads/2022/11/facebook.png" /></a>
		<a href="https://www.youtube.com/channel/UCIlppGCoI_F7Zd4p71U0hsA" target="_blank"><img style="width: 20px;margin-top: 5px;" src="<?php echo get_bloginfo('url') ?>/wp-content/uploads/2022/11/youtube.png" /></a>
		</span>	
		</div>
		</div>
	</body>
</html>
