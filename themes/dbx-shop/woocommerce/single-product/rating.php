<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;
$status = $product->get_stock_status();
if($status == 'outofstock') return;
$review_ratings_enabled = wc_review_ratings_enabled();
if ( ! $review_ratings_enabled ) {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();
if($review_count == 1){
    $rec = 'review';
}
if ( $review_count > 1 ){
    $rec = ' review-uri';
}
if ( $review_count > 0 ) {
    $title = '<b>'.$average.'</b><a href="#recenzii">'.' '.$review_count.' '.$rec.'</a>';
    //$title = sprintf( __( 'Rated %s out of 5', 'woocommerce' ), $average );
  } else {
    $title = '<a href="#recenzii">Adaugă review</a>';
    $average = 0;
  }
  $rating_html  = '<div class="single-rating-dbx"><div class="star-rating">';
  $rating_html .= '<span style="width:' . ( ( $average / 5 ) * 100 ) . '%"><strong class="rating">' . $average . '</strong> ' . __( 'out of 5', 'woocommerce' ) . '</span>';
  $rating_html .= '</div>';
  $rating_html .= '<div class="title-rating-single">'.$title.'</div></div>';
  echo $rating_html;
 ?>
