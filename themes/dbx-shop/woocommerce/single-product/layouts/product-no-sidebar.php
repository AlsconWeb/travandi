<div class="product-container">
	<div class="product-main">
		<div class="row dbx-title"><?php do_action( 'dbx_single_title' ); ?></div>
		<div class="row content-row mb-0">

			<div class="product-gallery large-<?php echo flatsome_option( 'product_image_width' ); ?> col">
				<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				do_action( 'woocommerce_before_single_product_summary' );
				?>
			</div>

			<div class="product-info summary col-fit col entry-summary ">
				<div class="summary-dbx">
					<div class="summary1">
						<?php
						/**
						 * linked variations
						 * livrare
						 * beneficii
						 * ajutor
						 */
						do_action( 'dbx_single_product_summary1' );
						?>
					</div>
					<div class="summary2">
						<?php
						/**
						 * pret
						 * butoane
						 * beneficii
						 */
						do_action( 'dbx_single_product_summary2' );
						?>
					</div>
				</div>
				<?php
				/**
				 * woocommerce_single_product_summary hook
				 *
				 * @hooked woocommerce_template_single_title - 5
				 * @hooked woocommerce_template_single_rating - 10
				 * @hooked woocommerce_template_single_price - 10
				 * @hooked woocommerce_template_single_excerpt - 20
				 * @hooked woocommerce_template_single_add_to_cart - 30
				 * @hooked woocommerce_template_single_meta - 40
				 * @hooked woocommerce_template_single_sharing - 50
				 */
				//do_action( 'woocommerce_single_product_summary' );
				do_action( 'dbx_action_on_product' );
				?>

			</div>

		</div>
		<div class="promo-single">
			<div class="container">
				<?php do_action( 'dbx_single_promo' ); ?>
			</div>
		</div>
		<div class="custom_upsells_dbx">
			<div class="container">
				<?php
				//	    do_action( 'dbx_bump_products' );
				echo do_shortcode( '[iconic_wsb_fbt]' );
				?>
			</div>
		</div>
	</div>
	<div class="product-footer">
		<div class="container">
			<?php
			/**
			 * woocommerce_after_single_product_summary hook
			 *
			 * @hooked woocommerce_output_product_data_tabs - 10
			 * @hooked woocommerce_upsell_display - 15
			 * @hooked woocommerce_output_related_products - 20
			 */
			do_action( 'woocommerce_after_single_product_summary' );
			?>
		</div>
	</div>
</div>
