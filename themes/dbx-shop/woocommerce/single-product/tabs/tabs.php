<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );
//var_dump($product_tabs);
global $product;
$desc = $product->get_description();
//var_dump($desc);
$product_attributes = $product->get_attributes();
$show_att = false;
if(!empty($product_attributes)){
	$show_att = true;
}
$video1 = get_post_meta($product->get_id(), '_product_video', true);
//var_dump($video1);
$attachs = get_post_meta($product->get_id(), 'wcpoa_attachments_id', true);
$show_attachs = false;
if(!empty($attachs['0'])){
	$show_attachs = true;
}
$consumabile = dbx_get_consumabile();
$accesorii = dbx_get_accesorii();
if ( ! empty( $product_tabs ) ) : ?>
    <div class="top-product-info">
        <div class="container">
            <?php foreach ( $product_tabs as $key => $product_tab ) : ?>
            <?php if($key == 'description'){ ?>
            <a class="tab-name-info descriere" href="#descriere" data-scroll="descriere">Descriere</a>
            <?php } ?>
			<?php if(($key == 'accesorii') && !empty($accesorii)){ ?>
            <a class="tab-name-info accesorii" href="#accesorii" data-scroll="accesorii">Accesorii</a>
            <?php } ?>
			<?php if(($key == 'consumabile') && !empty($consumabile)){ ?>
            <a class="tab-name-info consumabile" href="#consumabile" data-scroll="consumabile">Consumabile</a>
            <?php } ?>
            <?php if(($key == 'caracteristici_produs') && ($show_att == true)){ ?>
            <a class="tab-name-info caracteristici" href="#caracteristici" data-scroll="caracteristici">Caracteristici</a>
            <?php } ?>
			<?php if($key == 'ux_video_tab'){ ?>
            <a class="tab-name-info video" href="#video" data-scroll="video">Video</a>
            <?php } ?>
			<?php /* if(($key == 'wcpoa_product_tab') && ($show_attachs == true)){ 
			?>
            <a class="tab-name-info documentatie_pdf" href="#documentatie_pdf" data-scroll="documentatie_pdf">Documentație PDF</a>
            <?php } */?>
            <?php if($key == 'reviews'){ ?>
            <a class="tab-name-info recenzii" href="#recenzii" data-scroll="recenzii">Recenzii</a>
            <?php } ?>
            <?php endforeach; ?>
			
        </div>
    </div>
    <div class="container-produs">
	<div class="info-produs">
		<?php foreach ( $product_tabs as $key => $product_tab ) : //var_dump($product_tab);?>
		<?php if($key == 'description'){ ?>
		<section class="dbx-section" id="descriere" data-anchor="descriere">
		    <h3 class="tab-name">Descriere</h3>
			<div class="dbx-section-container dbx-collapse-offset">
		    <?php 
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
			?> 
			</div>	
			<button id="button_dbx_show">Vezi mai mult <i class="fa fa-arrow-down"></i></button>
			<button id="button_dbx_hide">Vezi mai putin <i class="fa fa-arrow-up"></i></button>
		</section>
		<?php } ?>
		<?php if(($key == 'caracteristici_produs') && ($show_att == true)){ ?>
		<section class="dbx-section" id="caracteristici" data-anchor="caracteristici">
		    <h3 class="tab-name">Caracteristici</h3>
			<div class="dbx-section-container dbx-collapse-offset">
		    <?php
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>
			</div>	
			<button id="button_dbx_show1">Vezi mai mult <i class="fa fa-arrow-down"></i></button>
			<button id="button_dbx_hide1">Vezi mai putin <i class="fa fa-arrow-up"></i></button>
		</section>
		<?php } ?>
		<?php if(($key == 'accesorii') && !empty($accesorii)){ ?>
		<section id="accesorii" data-anchor="accesorii">
		    <?php 
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>    
		</section>
		<?php } ?>
		<?php if(($key == 'consumabile') && !empty($consumabile) ) { ?>
		<section id="consumabile" data-anchor="consumabile">
		    <?php 
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>    
		</section>
		<?php } ?>
		<?php if($key == 'ux_video_tab'){ ?>
		<section class="dbx-section" id="video" data-anchor="video">
		    <?php 
				if ( isset( $product_tab['callback'] ) ) {
					echo '<h3 class="tab-name">Video</h3>';
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
			?> 
		</section>
		<?php } ?>
		<?php if(($key == 'wcpoa_product_tab') && ($show_attachs == true)){ ?>
		<section id="documentatie_pdf" data-anchor="documentatie_pdf">
		    <?php 
				if ( isset( $product_tab['callback'] ) ) {
					echo '<h3 class="documentatie_title tab-name">Documentație PDF</h3>';
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>    
		</section>
		<?php } ?>
		<?php if($key == 'reviews'){ ?>
		<section id="recenzii" data-anchor="recenzii">
		    <h3 class="tab-name">Recenzii</h3>
		    <?php
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?> 
		</section>
		<?php } ?>
		<?php endforeach; ?>
	</div>
	</div>
<?php endif; ?>