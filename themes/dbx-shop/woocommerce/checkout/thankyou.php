<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="row">
<?php if ( $order ) :
	$modalitate_plata = $order->get_payment_method_title();
	$plataz = $order->get_payment_method();
	//var_dump($plataz);
	$transport = $order->get_shipping_method();
	$country = $order->get_billing_country();
	$state = $order->get_billing_state();
	$country2 = $order->get_shipping_country();
	$state2 = $order->get_shipping_state();
	$shipping_phone = $order->get_shipping_phone();
	if(!$shipping_phone){
		$shipping_phone = $order->get_billing_phone();
	}
	$next = '';
	//var_dump($modalitate_plata);
	switch($modalitate_plata){
		case 'Ramburs la curier':
			$next = 'Vei plăti produsele în momentul ridicării coletului.';
			break;
		case 'Plăteşte cu cardul':
			$next = 'Plata cu cardul a fost acceptată și în curând te vei putea bucura de produsele pe care le-ai ales.';
			break;	
		case 'Plăteşte prin Ordin de plata / Transfer bancar':
			$next = 'Dupa plasarea comenzii, vei primi prin email factura proforma cu toate detaliile de plata.';	
			break;
	}
	if($plataz == 'oney3x4x'){
		$next = 'Plata în rate Oney a fost acceptată și în curând te vei putea bucura de produsele pe care le-ai ales.';
		$modalitate_plata = 'Rate Oney';
	}
	do_action( 'woocommerce_before_thankyou', $order->get_id() ); ?>
	<?php if ( $order->has_status( 'failed' ) ) : ?>
	<div class="large-12 col order-failed">
			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>
			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>
		</div>
	<?php else : ?>
	<div class="large-12 col dbx_thankyou">
		<h2 style="text-align:center;padding: 20px 0;">Îți mulțumim pentru comandă, <?php echo $order->get_billing_first_name();?> <?php echo $order->get_billing_last_name();?></h2>
		<p style="text-align:center;margin-bottom:25px;">Comanda <b>#<?php echo $order->get_order_number();?></b> a fost preluată și va fi procesată în cel mai scurt timp.</p>
		<div class="top_thankyou">
		<div class="thankyou_payment">
			<h3>Detalii Plată </h3>
			<p style="margin-bottom:5px;"><span>Modalitate de plată: </span><span><b><?php echo $modalitate_plata;?></b></span></p>
			<div class="what_next">
				<p style="margin-bottom:5px;"><b>Ce urmează?</b></p>
				<p style="margin-bottom:5px;"><?php echo $next;?></p>
			</div>
		</div>	
		<div class="thankyou_payment">
			<h3>Detalii Transport </h3>
			<p style="margin-bottom:5px;"><span>Modalitate de livrare: </span><span><b><?php echo $transport;?></b></span></p>
			<div class="what_next">
				<p style="margin-bottom:5px;"><b>Ce urmează?</b></p>
				<p style="margin-bottom:5px;">Vei fi notificat prin SMS și e-mail atunci când comanda ta va fi preluată de curier.</p>
				<?php if ( is_user_logged_in() ) : ?>
					<p style="margin-bottom:5px;">Între timp, poți verifica statusul comenzii tale direct în contul tău.</p>
				<?php endif; ?>
			</div>
				</div>
		</div>	
    <?php
    $get_payment_method = $order->get_payment_method();
    $get_order_id       = $order->get_id();
    ?>
    <?php do_action( 'woocommerce_thankyou_' . $get_payment_method, $get_order_id ); ?>
    <?php do_action( 'woocommerce_thankyou', $get_order_id ); ?>
		<div class="top_thankyou">
		<div class="thankyou_payment">
			<h3>Adresa de Facturare</h3>
			<?php if($order->get_billing_company()): ?>
			<p style="margin-bottom:5px;"><b>Nume Firmă: </b><?php echo $order->get_billing_company();?></p>
			<?php endif;?>
			<p style="margin-bottom:5px;"><b>Nume: </b><?php echo $order->get_billing_last_name();?> <?php echo $order->get_billing_first_name();?></p>
			<p style="margin-bottom:5px;"><b>Județ: </b><?php echo WC()->countries->get_states( $country )[$state];?></p>
			<p style="margin-bottom:5px;"><b>Localitate: </b><?php echo $order->get_billing_city();?></p>
			<p style="margin-bottom:5px;"><b>Adresa: </b><?php echo $order->get_billing_address_1();?><?php echo $order->get_billing_address_2();?></p>
			<p style="margin-bottom:5px;"><b>Telefon: </b><?php echo $order->get_billing_phone();?></p>
		</div>	
		<div class="thankyou_payment">
			<h3>Adresa de Livrare</h3>
			<p style="margin-bottom:5px;"><b>Nume: </b><?php echo $order->get_shipping_last_name();?> <?php echo $order->get_billing_first_name();?></p>
			<p style="margin-bottom:5px;"><b>Județ: </b><?php echo WC()->countries->get_states( $country2 )[$state2];?></p>
			<p style="margin-bottom:5px;"><b>Localitate: </b><?php echo $order->get_shipping_city();?></p>
			<p style="margin-bottom:5px;"><b>Adresa: </b><?php echo $order->get_shipping_address_1();?><?php echo $order->get_shipping_address_2();?></p>
			<p style="margin-bottom:5px;"><b>Telefon: </b><?php echo $shipping_phone;?></p>
			</div>
				</div>
		</div>	
    </div>
	<?php endif; ?>
	<?php else : ?>
		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
<?php endif; ?>	
</div>

