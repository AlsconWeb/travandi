<?php
/*
Template name: Template Informatii - DBX
*/
get_header();
//do_action( 'flatsome_before_page' ); ?>
<div id="content" class="content-area page-wrapper" role="main">
	<div class="row row-main">
		<div class="large-3 col dbx_sidebar">
			<div class="col-inner">	
				<?php get_sidebar(); ?>
			</div>
		</div>
		<div class="large-9 col dbx_content">
			<div class="col-inner">		
				<header class="entry-header">
					<h1 class="entry-title mb uppercase"><?php the_title(); ?></h1>
				</header>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php do_action( 'flatsome_before_page_content' ); ?>
						<?php the_content(); ?>
					<?php do_action( 'flatsome_after_page_content' ); ?>
				<?php endwhile; // end of the loop. ?>
			</div>
		</div>
	</div>
</div>
<?php
do_action( 'flatsome_after_page' );
get_footer();
?>