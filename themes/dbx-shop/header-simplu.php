<div id="masthead" class="header-main <?php header_inner_class('main'); ?>">
      <div class="header-inner flex-row container <?php flatsome_logo_position(); ?>" role="navigation">
           <!-- Logo -->
          <div id="logo" class="flex-col logo">
            <?php get_template_part('template-parts/header/partials/element','logo'); ?>
          </div>
          <!-- Right Elements -->
          <div class="flex-col hide-for-medium flex-right">
           <ul class="header-nav header-nav-main nav nav-right  nav-spacing-medium nav-uppercase">
               <li>
              <div class="header-button">
            <a href="/contul-meu/" class="nav-top-link nav-top-not-logged-in icon button circle is-outline is-small">
            <i class="icon-user"></i><span> Contul Meu</span>
            </a>

            </div>

            </li>
            <li class="header-divider"></li>
            <li class="cart-item has-icon">
            <a href="/cos" title="Coș" class="header-cart-link is-small">
            <span class="header-cart-title">
           <span class="cart-price"><span class="woocommerce-Price-amount amount">67,50&nbsp;<span class="woocommerce-Price-currencySymbol">lei</span></span> <small class="tax_label">(inclusiv TVA)</small></span>
            </span>

    		<span class="cart-icon image-icon">
			<strong>1</strong>
		</span>
		
  </a>


</li>
            </ul>
          </div>

          <!-- Mobile Right Elements -->
          <div class="flex-col show-for-medium flex-right">
            <ul class="mobile-nav nav nav-right ">
                <li>
              <div class="header-button">
            <a href="/contul-meu/" class="nav-top-link nav-top-not-logged-in icon button circle is-outline is-small" >
            <i class="icon-user"></i>
            </a>

            </div>

            </li>
              <li class="header-divider"></li><li class="cart-item has-icon">

      <a href="/cos" title="Coș" class="header-cart-link is-small">
  
    										<span class="cart-icon image-icon">
			<strong>2</strong>
		</span>
  </a>

</li>
            </ul>
          </div>
      </div>
      <?php if(get_theme_mod('header_divider', 1)) { ?>
      <div class="container"><div class="top-divider full-width"></div></div>
      <?php }?>
</div>
<?php
	function flatsome_checkout_breadcrumb_class($endpoint){
		$classes = array();
		if($endpoint == 'cart' && is_cart() ||
			$endpoint == 'checkout' && is_checkout() && !is_wc_endpoint_url('order-received') ||
			$endpoint == 'order-received' && is_wc_endpoint_url('order-received')) {
			$classes[] = 'current';
		} else{
			$classes[] = 'hide-for-small';
		}
		return implode(' ', $classes);
	}
	$steps = get_theme_mod('cart_steps_numbers', 0);
?>

<div class="checkout-page-title page-title">
	<div class="page-title-inner flex-row medium-flex-wrap container">
	  <div class="flex-col flex-grow medium-text-center">
	 	 <nav class="breadcrumbs flex-row flex-row-center heading-font checkout-breadcrumbs text-center strong <?php echo get_theme_mod('cart_steps_size','h2'); ?> <?php echo get_theme_mod('cart_steps_case','uppercase'); ?>">
  	   <a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="<?php echo flatsome_checkout_breadcrumb_class('cart'); ?>">
   			<?php if($steps) { echo '<span class="breadcrumb-step hide-for-small">1</span>'; } ?>
  	   	<?php _e('Shopping Cart', 'flatsome'); ?>
  	   	</a>
  	   <span class="divider hide-for-small"><?php echo get_flatsome_icon('icon-angle-right');?></span>
  	   <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="<?php echo flatsome_checkout_breadcrumb_class('checkout') ?>">
   			<?php if($steps) { echo '<span class="breadcrumb-step hide-for-small">2</span>'; } ?>
  	   	<?php _e('Checkout details', 'flatsome'); ?>
  	   </a>
  	   <span class="divider hide-for-small"><?php echo get_flatsome_icon('icon-angle-right');?></span>
  	   <a href="#" class="no-click <?php echo flatsome_checkout_breadcrumb_class('order-received'); ?>">
  	   	<?php if($steps) { echo '<span class="breadcrumb-step hide-for-small">3</span>'; } ?>
  	   	<?php _e('Order Complete', 'flatsome'); ?>
  	   </a>
		 </nav>
	  </div>
	</div>
</div>
