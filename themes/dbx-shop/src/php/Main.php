<?php
/**
 * Main theme class.
 *
 * @package travandi/dbx-shop
 */

namespace Travandi\DbxShop;

use WC_Product;

/**
 * Main class file.
 */
class Main {

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init Hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_filter( 'iconic_wsb_fbt_thumbnail_size', [ $this, 'change_image_size_in_woo_sales_booster' ], 20, 3 );
	}

	/**
	 * Change image size in woocommerce plugin sales booster.
	 *
	 * @param string|array $size         Default: array( 60 x 60 ).
	 * @param WC_Product   $bump_product The product shown in the FBT section.
	 * @param WC_Product   $product      The product shown on the page.
	 *
	 * @return int[]
	 */
	public function change_image_size_in_woo_sales_booster( $size, $bump_product, $product ): array {
		return [ 150, 150 ];
	}
}
