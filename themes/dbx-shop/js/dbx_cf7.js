jQuery(($) => {
    $(".retur_contravaloare").hide();
    $("input[name='radio-372']").click(function() {
    if($(this).val()=="Returnarea contravalorii") {
        $(".retur_contravaloare").show();
        $(".produs_la_schimb").hide();
    } 
    if($(this).val()=="Înlocuirea cu alte produse (înlocuirea produsului cu alt produs comandat sau ce urmează să-l comand de o valoare cel puțin egală cu produsul returnat, cu obligația de a achita în avans diferența de preț)") {
        $(".retur_contravaloare").hide();
        $(".produs_la_schimb").show();
    }
});
});