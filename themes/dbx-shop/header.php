<!DOCTYPE html>
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="ie9 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="ie8 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="<?php flatsome_html_classes(); ?>"> <!--<![endif]-->
<head>
	<meta name="theme-color" content="#0DA876" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php
	/*
	if(is_product()): 
        $id = get_the_ID();
        $product = wc_get_product($id);
        if(!$product) return;
        $price = wc_get_price_including_tax( $product );
        ?>
        <script type="text/javascript" src="https://assets-staging.oney.io/build/loader.min.js"></script>
        <script>
        function showOneyWidget () {
            var price = <?php echo $price; ?>;
            let options = {        
                country: "RO",
                language: "RO",
                merchant_guid: "680ddd7b-4c31-471b-9bd0-96b74f353286",
                payment_amount: price,
                filter_by: "filters",
                filters: [
                    {
                        payment_mode: '3x',
                        is_free: true
                    },
                    {
                        payment_mode: '4x',
                        is_free: true
                    }
                    ]
            };
			options.errorCallback = function (status, response) {
				console.log(status + " - " + response);
			}
            loadOneyWidget(function () {
                oneyMerchantApp.loadSimulationPopin({ options });
            });
        }
        </script>
        <?php endif;
	*/
	?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action( 'flatsome_after_body_open' ); ?>
<?php wp_body_open(); ?>
<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'flatsome' ); ?></a>
<div id="wrapper">
	<?php do_action( 'flatsome_before_header' ); ?>
	<header id="header" class="header <?php flatsome_header_classes(); ?>">
		<div class="header-wrapper">
		    	<?php  
    if( is_page( 'cos' ) || is_page( 'finalizare-comanda' ) ) {
        get_template_part( 'header-simplu', 'wrapper' );
    } else {
		//echo '<div class="header-top-mobil"><img src="/wp-content/uploads/2021/12/banner-top-mobile.png" /></div>';
       get_template_part( 'template-parts/header/header', 'wrapper' );
    }
?>
		</div>
	</header>
	<?php do_action( 'flatsome_after_header' ); ?>
	<main id="main" class="<?php flatsome_main_classes(); ?>">