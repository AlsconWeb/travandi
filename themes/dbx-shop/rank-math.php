<?php
//pagina produs
add_filter( 'rank_math/frontend/description', 'custom_product_meta_dbx', 5);
function custom_product_meta_dbx( $description ) {
	if(is_product_category()){
	$id = get_the_id();
	$cate = get_queried_object();
	$cateID = $cate->term_id;
	$cateName = $cate->name;
	//$descriere = $cate->description;
	$descriere = get_term_meta($cateID, 'rank_math_description', true);
	//var_dump($descriere);
	$count = $cate->count;
	$description = 'Cumpara acum '.$cateName.' la cele mai mici preturi! Beneficiezi de produse originale 100%. Livrare Rapida. Service si garantie. Plata in 3 rate. Deschidere colet.';
	if(!empty($descriere)){
		$description = $descriere;
	}
	return $description;
	}
	if(is_product()){
	$id = get_the_id();
	$product = wc_get_product($id);
	$name = $product->get_name();
	$price = wc_get_price_including_tax( $product );
	$greutate_produs = $product->get_weight();
	$transport_settings = get_option('woocommerce_metoda-livrare_settings');
	$cost_kg = $transport_settings['cost_kg'];
	$tarif_implicit = $transport_settings['tarif_implicit'];
	$prag_special = $transport_settings['prag_transport_fix'];
	$prag_gratuit = $transport_settings['prag_gratis'];
	$cost_special = $transport_settings['cost_transport_fix'];
	$greutate_max = $transport_settings['greutate_max'];
	if($greutate_produs > 0){
		$greutate = $greutate_produs;
	}
	else{
		$greutate = 1;
	}
	$transport = $tarif_implicit + ($cost_kg * $greutate);
	if($price > $prag_special){
		$transport = $cost_special;
	}
	if(($price > $prag_gratuit) && ($greutate < $greutate_max)){
		$transport = 0;
	}
	if($transport == 0){
		$mesaj_transport = 'Livrare Gratuita. ';
	}
	else{
		$mesaj_transport = 'Livrare Rapida. ';
	}
	$open = '';
	if($price > 399){
		$open = 'Deschidere colet';
	}
	$description = 'Cumpara acum '.$name.' la doar '.$price.' lei si beneficiezi de produs original 100%. Service si garantie. Plata in 3 Rate. '.$mesaj_transport.$open;
	return $description;
	}
	global $wp_query;
	$query = $wp_query->query_vars;
	$producatori = isset($query['producatori']) ? $query['producatori'] : '';
	if(empty($producatori)) return;
	//if(!$query['taxonomy'] || empty($query['taxonomy'])) return;
	//var_dump($query);
	//var_dump($query['producatori']);
	//exit;
	if(!empty($query) && ($query['taxonomy'] == 'producatori')){
		$producator = wp_get_post_terms(get_the_id(), 'producatori');
		if($producator){
		$id = $producator[0]->term_id;
		$name = $producator[0]->name;
		$count = $producator[0]->count;
		$description = 'Cumpara acum produse originale '.$name.' la cele mai mici preturi! Beneficiezi de produse originale 100%. Livrare Rapida. Service si garantie. Plata in 3 rate. Deschidere colet.';
		return $description;
		}
	}
}
//categorie produs
//add_filter( 'rank_math/frontend/description', 'custom_category_meta_dbx', 10);
function custom_category_meta_dbx( $description ) {
	if(is_product_category()){
	$id = get_the_id();
	$cate = get_queried_object();
	$cateID = $cate->term_id;
	$cateName = $cate->name;
	$count = $cate->count;
	$description = 'Cumpara acum '.$cateName.' la cele mai mici preturi! Peste '.$count.' produse in stoc. Beneficiezi de produse originale 100%. Livrare Rapida. Service si garantie. Plata in 3 rate. Deschidere colet.';
	return $description;
	}
}
//producatori produs
//add_filter( 'rank_math/frontend/description', 'custom_producatori_meta_dbx', 15);
function custom_producatori_meta_dbx( $description ) {
	global $wp_query;
	$query = $wp_query->query_vars;
	if(($query['taxonomy'] != 'producatori')) return;
	$producator = wp_get_post_terms(get_the_id(), 'producatori');
	if(!$producator) return;
	$id = $producator[0]->term_id;
	$name = $producator[0]->name;
	$count = $producator[0]->count;
	$description = 'Cumpara acum produse originale '.$name.' la cele mai mici preturi! Peste '.$count.' produse in stoc. Beneficiezi de produse originale 100%. Livrare Rapida. Service si garantie. Plata in 3 rate. Deschidere colet.';
	return $description;
}