<?php
/**
 * Theme dbx-shop.
 *
 * @package travandi/dbx-shop
 */

use Travandi\DbxShop\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();

// Add custom Theme Functions here
/**
 * Allow HTML in term (category, tag) descriptions
 */

foreach ( [ 'pre_term_description' ] as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
	if ( ! current_user_can( 'unfiltered_html' ) ) {
		add_filter( $filter, 'wp_filter_post_kses' );
	}
}

foreach ( [ 'term_description' ] as $filter ) {
	remove_filter( $filter, 'wp_kses_data' );
}
//adauga cod produs sub titlu
//add_action('woocommerce_single_product_summary','dbx_product_sku', 6);
function dbx_product_sku() {
	global $product;
	$sku = $product->get_sku();
	$ean = $product->get_meta( '_ean_dbx' );
	if ( $sku || $ean ) {
		if ( $sku ) {
			$cod_produs = $sku;
		}
		if ( $ean ) {
			$cod_produs = $ean;
		}
		if ( $sku && $ean ) {
			$cod_produs = $ean;
		}
	}
	echo '<span class="cod-produs">Cod Produs: ' . $cod_produs . '</span>';
}

//CHANGE IMAGE QUALITY TO HIGHEST
add_filter( 'jpeg_quality', function ( $arg ) {
	return 100;
} );
//RENAME HOME IN BREADCRUMBS
add_filter( 'woocommerce_breadcrumb_defaults', 'dbx_change_breadcrumb_home_text', 20 );
function dbx_change_breadcrumb_home_text( $defaults ) {
	$defaults['home'] = 'Acasă';

	return $defaults;
}

//schimba upsell products
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'produse_promovate', 'woocommerce_upsell_display', 5 );
// ascunde cantitate in pagina produsului
add_filter( 'woocommerce_quantity_input_min', 'hide_woocommerce_quantity_input', 10, 2 );
add_filter( 'woocommerce_quantity_input_max', 'hide_woocommerce_quantity_input', 10, 2 );
function hide_woocommerce_quantity_input( $quantity, $product ) {
	// only on the product page
	if ( ! is_product() ) {
		return $quantity;
	}

	return 1;
}

add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_urll', 10 );
function wc_empty_cart_redirect_urll() {
	return '/';
}

//add_filter('woocommerce_product_get_rating_html', 'your_get_rating_html', 10, 2);
function your_get_rating_html( $rating_html, $rating ) {
	if ( $rating > 0 ) {
		$title = sprintf( __( 'Rated %s out of 5', 'woocommerce' ), $rating );
	} else {
		$title  = 'Încă nu are recenzii';
		$rating = 0;
	}
	$rating_html = '<div class="star-rating" title="' . $title . '">';
	$rating_html .= '<span style="width:' . ( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . __( 'out of 5', 'woocommerce' ) . '</span>';
	$rating_html .= '</div>';

	return $rating_html;
}

add_filter( 'woocommerce_default_address_fields', 'dbx_woocommerce_field_order2', 20, 1 );
function dbx_woocommerce_field_order2( $fields ) {
	$fields['state']['priority']     = 80;
	$fields['state']['class']        = [ 'form-row-first' ];
	$fields['city']['priority']      = 85;
	$fields['city']['class']         = [ 'form-row-last' ];
	$fields['address_1']['priority'] = 87;
	$fields['address_1']['label']    = 'Adresa Completă';
	$fields['address_1']['class']    = [ 'form-row-wide' ];
	unset( $fields['postcode'] );
	unset( $fields['address_2'] );

	return $fields;
}

add_filter( 'woocommerce_billing_fields', 'dabex_move_checkout_phone_field', 20, 1 );
function dabex_move_checkout_phone_field( $address_fields ) {
	$address_fields['billing_email']['priority'] = 2;
	$address_fields['billing_email']['class']    = [ 'form-row-first' ];
	$address_fields['billing_phone']['priority'] = 4;
	$address_fields['billing_phone']['class']    = [ 'form-row-last' ];

	return $address_fields;
}

add_action( 'wp_footer', 'format_checkout_billing_phone' );
function format_checkout_billing_phone() {
	if ( is_checkout() && ! is_wc_endpoint_url() ) :
		?>
		<script type="text/javascript">
			jQuery( function( $ ) {
				$( '#billing_phone' ).on( 'input focusout', function() {
					var p = $( this ).val();
					p = p.replace( /[^0-9]/g, '' );
					$( this ).val( p );
				} );
			} );
		</script>
	<?php
	endif;
}

add_action( 'woocommerce_checkout_process', 'custom_validate_billing_phone' );
function custom_validate_billing_phone() {
	$is_correct = preg_match( '/^[0-9 \-]{10}/i', $_POST['billing_phone'] );
	if ( $_POST['billing_phone'] && ! $is_correct ) {
		wc_add_notice( __( 'Numarul de telefon este format din <strong>10 caractere</strong>.' ), 'error' );
	}
}

//Autocomplete Reg Com And Nume Firma based on CUI - OPENAPI.RO
add_action( 'wp_footer', 'change_checkout_fields_script' );
function change_checkout_fields_script() {
	// Only checkout page
	if ( is_checkout() && ! is_wc_endpoint_url() ):
		?>
		<script type="text/javascript">
			jQuery( function( $ ) {
				var tip_pers = 'pers-jur';
				var cui_field = $( '#cui' );
				cui_field.on( 'change', function() {
					$( 'body' ).trigger( 'update_checkout' );
					var reg_com = $( '#nr_reg_com' );
					var nume_firma = $( '#billing_company' );
					var address = $( '#billing_address_1' );
					var cod_postal = $( '#billing_postcode' );
					if ( cui_field.val() === '' ) {
						reg_com.val( '' );
						reg_com.prop( 'readonly', false );
						nume_firma.val( '' );
						nume_firma.prop( 'readonly', false );
					}
					if ( cui_field.val() !== '' ) {
						const data = {
							'key': '6dd9c1e85c43c0b645a2134e7b916bba219fb68e',
							'cui': cui_field.val()
						};
						const params = new URLSearchParams( data );
						var url = 'https://www.infocui.ro/system/api/data?';
						url += params;
						request = $.ajax( {
							type: 'GET',
							//headers: {"x-api-key": "QH-t8mkch_jNyqXo3tHs28s4t29iaKdn4-jqDuNPtd7YxTPryg"},
							url: url
						} );
						request.done( function( response, textStatus, jqXHR ) {
							response = JSON.parse( response );
							if ( response.data.cod_inmatriculare ) {
								reg_com.val( response.data.cod_inmatriculare );
								reg_com.prop( 'readonly', true );
								nume_firma.val( response.data.nume );
								nume_firma.prop( 'readonly', true );
								//address.val(response.data.adresa);
								//cod_postal.val(response.cod_postal);
							}
						} );
					}
				} );
			} );
		</script>
	<?php
	endif;
}

function card_online( $id ) {
	if ( $id == 'vivawallet_native' ) {
		echo '<span class="card_recomandat">recomandat</span>';
	}
}

add_action( 'dbx_payment_label', 'card_online' );

function enable_widgets() {
	register_sidebar(
		[
			'name' => 'Secondary Sidebar',
			'id'   => 'second-sidebar',

		] );
}

add_action( 'widgets_init', 'enable_widgets' );
function dbx_cf7_load_scripts() {
	global $post;
	if ( is_page() || is_single() ) {
		switch ( $post->post_name ) {
			case 'returnare-produse':
				wp_enqueue_script( 'returnare-produse', get_stylesheet_directory_uri() . '/js/dbx_cf7.js', [ 'jquery' ], '1.0.5', true );
				break;
		}
	}
}

add_action( 'wp_enqueue_scripts', 'dbx_cf7_load_scripts' );
/*
add_action( 'woocommerce_before_cart_table', 'dabex_cart_notice_transport_gratuit', 10 );
function dabex_cart_notice_transport_gratuit(){
	$amount_transport= 399;
	$current = WC()->cart->subtotal;
	if ( $current < $amount_transport ) {
	     $added_text = '<span class="transport-gratuit-cart">'.'Cumpără produse de încă <span class="amount-tg">'.wc_price( $amount_transport - $current ).'</span> și beneficiezi de <b>TRANSPORT GRATUIT</b>'.'</span>';
        $notice = $added_text;   
    wc_print_notice( $notice, 'notice' );
    }
	else {
		//$added_text = esc_html__('Felicitări! Beneficiezi de TRANSPORT GRATUIT! ', 'woocommerce' );
		$added_text = '<span class="transport-gratuit-cart">Felicitări! Beneficiezi de <b>TRANSPORT GRATUIT!</b></span>';
    $notice = $added_text;
    wc_print_notice( $notice, 'notice' );
	}
}
*/
function dbx_hide_shipping_when_free_is_available( $rates, $package ) {
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
		return $rates;
	}
	$has_free_shipping = false;
	$applied_coupons   = WC()->cart->get_applied_coupons();
	foreach ( $applied_coupons as $coupon_code ) {
		$coupon = new WC_Coupon( $coupon_code );
		if ( $coupon->get_free_shipping() ) {
			$has_free_shipping = true;
			break;
		}
	}
	if ( isset( $rates['free_shipping:2'] ) || $has_free_shipping ) {
		unset( $rates['flat_rate:1'] );
	}

	return $rates;

}

add_filter( 'woocommerce_package_rates', 'dbx_hide_shipping_when_free_is_available', 10, 2 );
//add_action('woocommerce_after_cart','show_cart_badges', 10);
//add_action('woocommerce_checkout_after_order_review','show_cart_badges', 10);
function show_cart_badges() {
	echo '<div class="cart-badges">';
	echo '<h2 class="cart-badges-title">De ce să cumperi de la noi?</h2>';
	echo '<div class="badges">';
	echo '<div class="badge-cart">';
	echo '<div class="container">';
	echo '<img src="/wp-content/uploads/2021/08/open-package.svg" />';
	echo '<span class="badge-cart-text"> Deschidere Colet la Livrare </span>';
	echo '</div>';
	echo '</div>';
	echo '<div class="badge-cart">';
	echo '<div class="container">';
	echo '<img src="/wp-content/uploads/2021/08/return.svg" />';
	echo '<span class="badge-cart-text"> 14 zile drept de <b>Retur Gratuit </b></span>';
	echo '</div>';
	echo '</div>';
	echo '<div class="badge-cart">';
	echo '<div class="container">';
	echo '<img src="/wp-content/uploads/2021/08/giftbox.svg" />';
	echo '<span class="badge-cart-text"> <b>Cadou SURPRIZĂ</b> - comenzi de minim 299 lei </span>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
}

add_action( 'dbx_before_footer', 'dbx_recent_views', 5 );
function dbx_recent_views() {
	if ( is_page( 'cos' ) || is_product() ) {
		return;
	}
	echo '<div class="dbx_before_footer">';
	echo '<div class="container">';
	echo do_shortcode( '[yith_similar_products title="RECENT VIZUALIZATE" view_all="Vezi toate &gt;&gt; " prod_type="viewed" similar_type="both" order="viewed" cat_most_viewed="no" cats_id="" slider="no" autoplay="no" dots="yes" num_post="5" num_columns="5"]' );
	echo '</div></div>';
}


####Adauga search SKU in pagina comenzi admin
add_filter( 'woocommerce_shop_order_search_results', 'bbloomer_order_search_by_sku', 9999, 3 );

function bbloomer_order_search_by_sku( $order_ids, $term, $search_fields ) {
	global $wpdb;
	if ( ! empty( $search_fields ) ) {
		$product_id = wc_get_product_id_by_sku( $wpdb->esc_like( wc_clean( $term ) ) );
		if ( ! $product_id ) {
			return $order_ids;
		}
		$order_ids = array_unique(
			$wpdb->get_col(
				$wpdb->prepare( "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_items WHERE order_item_id IN ( SELECT order_item_id FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key IN ( '_product_id', '_variation_id' ) AND meta_value = %d ) AND order_item_type = 'line_item'", $product_id )
			)
		);
	}

	return $order_ids;
}


add_filter( 'woocommerce_checkout_get_value', 'dbx_remove_autocomplete_checkout', 10, 2 );
function dbx_remove_autocomplete_checkout( $value, $input ) {
	$user = wp_get_current_user();
	if ( empty( $user ) ) {
		return;
	}
	$role = $user->roles;
	if ( empty( $role ) || ! $role ) {
		return;
	}
	if ( $role['0'] != 'shop_manager' ) {
		return;
	}
	$item_to_set_null = [
		'billing_first_name',
		'billing_last_name',
		'billing_company',
		'billing_address_1',
		'billing_address_2',
		'billing_city',
		'billing_postcode',
		'billing_country',
		'billing_state',
		// 'billing_email',
		'billing_phone',
		'cui',
		'nr_reg_com',
		'shipping_first_name',
		'shipping_last_name',
		'shipping_company',
		'shipping_address_1',
		'shipping_address_2',
		'shipping_city',
		'shipping_postcode',
		'shipping_country',
		'shipping_state',
	];
	if ( in_array( $input, $item_to_set_null ) ) {
		$value = '';
	}

	return $value;
}

add_action( 'admin_enqueue_scripts', 'remove_views_select2' );
function remove_views_select2( $hook ) {
	if ( ( $hook == 'post.php' || $hook == 'post-new.php' ) ) {
		wp_deregister_script( 'select2' );
		// wp_register_script( 'views-select2-script' , 'https://travandi.ro/wp-content/plugins/meta-box/js/select2/select2.min.js', array('jquery'), 3.2);
	}
}
